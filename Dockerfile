FROM eclipse-temurin:11-jdk

RUN apt-get update \
  && apt-get install -y ant git --no-install-recommends \
  && rm -rf /var/lib/apt/lists/*
